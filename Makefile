CXX = clang++
CXXFLAGS = -Wall -Werror -pedantic -std=c++17 -g
SRC = $(wildcard *.cpp)
OBJ = $(src:.cpp=.o)

viper: $(SRC)
	@echo 'Building viper using:'
	${CXX} ${CXXFLAGS} -o $@ $^
	@echo 'Done!'
