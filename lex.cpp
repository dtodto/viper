#include <iostream>
#include <regex>
#include <string>
#include <fstream>
#include <iterator>

#include "lex.hpp"

Lex::Lex()
{
	define_syntax();
	std::cout << "Syntax table initialized!" << std::endl;
}

void Lex::define_syntax()
{
	add_syntax(KEY, "elif");
	add_syntax(KEY, "if");
	add_syntax(KEY, "else");
	add_syntax(KEY, "for");
	add_syntax(KEY, "while");
	add_syntax(KEY,	"def");

	add_syntax(TYPE, "i32");
	add_syntax(TYPE, "u32");
	add_syntax(TYPE, "f32");
	add_syntax(TYPE, "string");
	add_syntax(TYPE, "char");

	add_syntax("OPAREN", "[)]");
	add_syntax("CPAREN", "[(]");
	add_syntax("ENDEXPR", ":");

	add_syntax("STRING", "\"[ a-zA-Z0-9]*\"");
	add_syntax("NUMBER", "[0-9]+");
	add_syntax("FLAOT", "[0-9]+.[0-9]+");

	add_syntax("PLUS", "[+]");
	add_syntax("MINUS", "[-]");
	add_syntax("MULT", "[*]");
	add_syntax("DIV", "[/]");
	add_syntax("EQUAL", "==");
	add_syntax("NOTEQ", "!=");
	add_syntax("ASSIGN", "[=]");

	add_syntax("ID", "[a-z][a-zA-Z0-9]*");
}

void Lex::add_syntax(std::string token, std::string expr)
{
	table.push_back(std::make_pair(token, expr));
}

void Lex::lex_file(const char *filename)
{
	//open in and out file
	std::ifstream lex_fileptr(filename);
	std::ofstream lexed_fileptr("lexed.txt");
	//checking if everything went OK with opening the files
	if(!lex_fileptr.is_open()) {
		std::cerr << "Could not open file" << filename;
		std::cerr << "Exiting..." <<std::endl;
		return;
	} else if (!lexed_fileptr.is_open()) {
		std::cerr << "Could not open outfile.\nExiting..." << std::endl;
		return;
	}

	//read line by line form lex_fileptr
	std::string line;
	while(std::getline(lex_fileptr, line)) {
		lexed_fileptr << lex(line);
	}
}

std::string Lex::lex(std::string &in)
{
	for(auto pair : table) {
		try {
			std::regex rgx(pair.second);
			auto begin = std::sregex_iterator(in.begin(), in.end(), rgx);
			auto end = std::sregex_iterator();
			for(std::sregex_iterator i = begin; i != end; i++) {
				std::smatch match = *i;
				std::cout << match.str() << std::endl;
			}
		}
		catch(const std::regex_error &e) {
			std::cout << "regex_error: " << e.what() << std::endl;
		}
	}
	return "ERROR(" + in + ")";
}

void Lex::start_repl()
{
	while(true) {
		std::cout << ">>> ";
		std::string input;
		std::getline(std::cin, input);
		if(input == "/quit" || input == "/q") {
			return;
		} else {
			//pass input as long as it's not empty
			//lex() function is consuming input variable making to shorter
			while(!input.empty()) {
				std::cout << lex(input) << std::endl;
			}
		}
	}
}
