#include "lex.hpp"

int main(int argc, char const *argv[])
{
	Lex lex;
	if(argc == 1) {
		std::cout << "Hello, world!" << std::endl;
		lex.start_repl();
	} else if(argc == 2) {
		std::cout << "Hello, world!" << std::endl;
		lex.lex_file(argv[1]);
		std::cout << "Done lexing!" << std::endl;
		return 0;
	}
}

/*
std::string lex(const std::string &in)
{
	std::vector<std::pair<std::string, std::string>> table;
	define_syntax(table);

	for(auto pair : table) {
		std::smatch match;
		try{
			if(std::regex_match(in, match, std::regex(pair.second))) {
				return pair.first + "(" + match.str() + ") ";
			} else {
				continue;
			}
		}
		catch(const std::regex_error &e) {
			std::cout << "regex_error: " << e.what() << std::endl;
		}
	}
	return "Error on lex()\n";
}


void add_entry(std::vector<std::pair<std::string, std::string>> &table,
	const std::string &key, const std::string &val)
{
		table.push_back(std::make_pair(key, val));
		return;
}

void define_syntax(std::vector<std::pair<std::string, std::string>> &table)
{
		add_entry(table, KEY, "if");
		add_entry(table, KEY, "elif");
		add_entry(table, KEY, "else");
		add_entry(table, KEY, "for");
		add_entry(table, KEY, "while");

		add_entry(table, TYPE, "i32");
		add_entry(table, TYPE, "u32");
		add_entry(table, TYPE, "f32");
		add_entry(table, TYPE, "string");
		add_entry(table, TYPE, "char");

		add_entry(table, OPAREN, "[)]");
		add_entry(table, CPAREN, "[(]");
		add_entry(table, "ENDEXPR", ":");

		add_entry(table, STRING, "\"[a-zA-Z0-9]*\"");
		add_entry(table, "NUMBER", "[0-9]+");
		add_entry(table, "FLAOT", "[0-9]+.[0-9]+");

		add_entry(table, OPERA, "[+]");
		add_entry(table, OPERA, "[-]");
		add_entry(table, OPERA, "[*]");
		add_entry(table, OPERA, "[/]");
		add_entry(table, OPERA, "[=]");
		add_entry(table, OPERA, "==");

		add_entry(table, ID, "[a-z][a-zA-Z0-9]*");
}

void table_debug(std::vector<std::pair<std::string, std::string>> table)
{
	for(auto i : table) {
		std::cout << i.first <<" "<< i.second << std::endl;
	}
}

void lex_file(const char *filename)
{
	std::cout << "stub" << std::endl;
	return;

	ifstream lex_fileptr(filename);
	if(!fileptr.is_open()) {
		std::cout << "Couldn't open file to lex.\nExiting...\n";
		return;
	}
	std::string line;
	while(std::getline(fileptr, line))

}
*/
