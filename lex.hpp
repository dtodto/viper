#ifndef LEX_HPP
#define LEX_HPP

#include <iostream>
#include <regex>
#include <fstream>
#include <string>

#define TYPE	"TYPE"
#define KEY		"KEYWORD"

class Lex
{
private:
	std::vector<std::pair<std::string, std::string>> table;
	void define_syntax();
	void add_syntax(std::string, std::string);
	std::string lex(std::string&);
public:
	Lex();
	void start_repl();
	void lex_file(const char*);
};
#endif
